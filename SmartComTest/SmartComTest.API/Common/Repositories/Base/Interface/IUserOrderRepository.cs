﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartComTest.API.Common.Repositories.Base.Interface
{
    public interface IUserOrderRepository<T> : IUserRepository<T>
        where T : class
    {
        void Create(T item);
        void Delete(Guid id);
    }
}
