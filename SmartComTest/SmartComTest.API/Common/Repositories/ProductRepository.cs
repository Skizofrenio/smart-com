﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Repositories.Base;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Common.Repositories
{
    public class ProductRepository : RepositoryBase<Product>
    {
        public ProductRepository(IApplicationDbContext context)
            : base(context)
        {
        }
        public override void Create(Product item)
        {
            db.Products.Add(item);
        }

        public override void Delete(Guid id)
        {
            Product product = db.Products.Where(p => p.Id == id).FirstOrDefault();
            if (product != null)
                db.Products.Remove(product);
        }

        public override Product Get(Guid id)
        {
            return db.Products.Where(p => p.Id == id).FirstOrDefault();
        }

        public override IEnumerable<Product> GetAll()
        {
            return db.Products;
        }

        public override void Update(Product item)
        {
            db.SetModified(item);
        }
    }
}