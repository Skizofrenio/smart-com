﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Datalayers;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartComTest.API.Controllers
{
    public class BaseController : ApiController
    {
        protected UserDatalayer Datalayer;

        public BaseController()
        {
            Datalayer = new UserDatalayer(new ApplicationDbContext());
        }

        public BaseController(IApplicationDbContext context)
        {
            Datalayer = new UserDatalayer(context);
        }
    }
}
