import Axios from 'axios'

export default {
  state: {
    products: null,
    pages: null
  },
  getters: {
    PRODUCTS: state => {
      return state.products
    },
    PAGES: state => {
      return state.pages
    },
  },
  mutations: {
    SET_PRODUCTS: (state, payload) => {
      state.products = payload.Products
      state.pages = payload.Pages
    },

    ADD_PRODUCT: (state, payload) => {
      state.products.push(payload)
    },
    REMOVE_PRODUCT: (state, id) => {
      state.products.forEach((product, index) => {
        if (product.Id === id) {
          state.products.splice(index, 1)
        }
      })
    },
    EDIT_PRODUCT: (state, payload) => {
      state.products.forEach((product, index) => {
        if (product.Id === payload.id) {
          product.Name = payload.name
          product.Price = payload.price
          product.Category = payload.category
        }
      })
    }
  },
  actions: {
    GET_PRODUCTS: async (context, page) => {
      let { data } = await Axios.get('https://localhost:44350/api/product/index?page=' + page)
      context.commit('SET_PRODUCTS', data)
    },

    SAVE_PRODUCT: async (context, product) => {
      const requestBody = product
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
      }
      
      let {data, status} = await Axios.post('https://localhost:44350/api/admin/product/add', requestBody, config)
      
      if (status === 200) 
        context.commit('ADD_PRODUCT', data)
    },

    DELETE_PRODUCT: async (context, id) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
      }
      
      let {status} = await Axios.delete('https://localhost:44350/api/admin/product/delete?id=' + id, config)
      
      if (status === 200) 
        context.commit('REMOVE_PRODUCT', id)
    },
    UPDATE_PRODUCT: async (context, payload) => {
      const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
      }
      let {status} = await Axios.put('https://localhost:44350/api/admin/product/update', payload, config)
      
      if (status === 200) 
        context.commit('EDIT_PRODUCT', payload)
    },
    SEARCH_PRODUCT: async (context, payload) => {
      let {data, status} = await Axios.get('https://localhost:44350/api/product/search?filter=' + payload.filter + '&searchString=' + payload.searchString)
      
      if (status === 200) 
        context.commit('SET_PRODUCTS', data)
    }
  }
}
