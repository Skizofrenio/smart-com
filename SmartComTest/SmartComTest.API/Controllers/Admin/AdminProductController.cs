﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Datalayers;
using SmartComTest.API.Models;
using SmartComTest.API.Models.BindingModels;
using SmartComTest.API.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SmartComTest.API.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/admin/product")]
    public class AdminProductController : AdminBaseController
    {
        public AdminProductController()
            : base()
        {

        }

        public AdminProductController(IApplicationDbContext context)
            : base(context)
        {

        }

        [Route("add")]
        [HttpPost]
        public IHttpActionResult Post(AddProductBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var product = new Product()
            {
                Category = model.Category,
                Name = model.Name,
                Price = model.Price,
            };

            Datalayer.Products.Create(product);
            Datalayer.Save();

            return Json(new ProductViewModel()
            {
                Id = product.Id,
                Name = product.Name,
                Category = product.Category,
                Code = product.Code,
                Price = product.Price,
            });
        }

        [Route("update")]
        [HttpPut]
        public IHttpActionResult Put(UpdateBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var product = Datalayer.Products.Get(model.Id);
            if (product == null)
                return BadRequest();

            product.Category = model.Category;
            product.Name = model.Name;
            product.Price = model.Price;

            Datalayer.Products.Update(product);
            Datalayer.Save();

            return Ok();
        }

        [Route("delete")]
        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            Datalayer.Products.Delete(id);
            Datalayer.Save();

            return Ok();
        }
    }
}