namespace SmartComTest.API.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using SmartComTest.API.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SmartComTest.API.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SmartComTest.API.Models.ApplicationDbContext context)
        {
            #region Role

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            roleManager.Create(new IdentityRole() { Name = "Admin" });
            roleManager.Create(new IdentityRole() { Name = "User" });

            #endregion

            #region Admin user

            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            var admin = new ApplicationUser()
            {
                UserName = "Admin",
                Email = "admin@admin.com",
                EmailConfirmed = true,
            };

            var result = userManager.Create(admin, "AdminPassword1!");

            if (result.Succeeded)
            {
                userManager.AddToRole(admin.Id, "Admin");
            }

            #endregion.
        }
    }
}
