﻿using SmartComTest.API.Common.Repositories.Base.Interface;
using SmartComTest.API.Common.Repositories.Base;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SmartComTest.API.Common.Context;

namespace SmartComTest.API.Common.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IUserOrderRepository<Order>
    {
        public OrderRepository(IApplicationDbContext context)
            : base(context)
        {
        }

        public override void Create(Order item)
        {
            db.Orders.Add(item);
        }

        public override void Delete(Guid id)
        {
            Order order = db.Orders.Where(o => o.Id == id).FirstOrDefault();
            if (order != null)
                db.Orders.Remove(order);
        }

        public override Order Get(Guid id)
        {
            return db.Orders
                        .Where(o => o.Id == id)
                        .Include(o => o.Customer)
                        .Include(o => o.Items)
                        .FirstOrDefault();
        }

        public override IEnumerable<Order> GetAll()
        {
            return db.Orders;
        }

        public override void Update(Order item)
        {
            db.SetModified(item);
        }
    }
}