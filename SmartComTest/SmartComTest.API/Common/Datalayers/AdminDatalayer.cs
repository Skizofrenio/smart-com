﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Repositories;
using SmartComTest.API.Common.Repositories.Base.Interface;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Common.Datalayers
{
    public class AdminDatalayer : BaseDatalayer
    {
        public AdminDatalayer(IApplicationDbContext context)
            : base(context)
        {
        }

        private IRepository<Product> productRepository;

        public IRepository<Product> Products
        {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(db);

                return productRepository;
            }
        }

        private IIdentityRepository<Customer> customerRepository;

        public IIdentityRepository<Customer> Customers
        {
            get
            {
                if (customerRepository == null)
                    customerRepository = new CustomerRepository(db);

                return customerRepository;
            }
        }

        private IRepository<Order> orders;

        public IRepository<Order> Orders
        {
            get
            {
                if (orders == null)
                    orders = new OrderRepository(db);

                return orders;
            }
        }
        
        private IRepository<OrderItem> orderItem;

        public IRepository<OrderItem> OrderItems
        {
            get
            {
                if (orderItem == null)
                    orderItem = new OrderItemRepository(db);

                return orderItem;
            }
        }
    }
}