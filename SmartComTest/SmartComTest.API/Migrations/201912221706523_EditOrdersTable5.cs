namespace SmartComTest.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditOrdersTable5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "OrderNumber", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "OrderNumber");
        }
    }
}
