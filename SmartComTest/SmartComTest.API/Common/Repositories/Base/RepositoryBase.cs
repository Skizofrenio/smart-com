﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Repositories.Base.Interface;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Common.Repositories.Base
{
    public abstract class RepositoryBase<T> : IUserRepository<T>, IRepository<T>
        where T : class
    {
        protected IApplicationDbContext db;

        public RepositoryBase(IApplicationDbContext context)
        {
            db = context;
        }
        public abstract void Create(T item);

        public abstract void Delete(Guid id);

        public abstract T Get(Guid id);

        public abstract IEnumerable<T> GetAll();

        public abstract void Update(T item);
        
        public IEnumerable<T> ToPagedList(IEnumerable<T> items, int pageNumber, int pageSize) 
            => items
                .Skip(pageSize * (pageNumber - 1)).Take(pageSize);
    }
}