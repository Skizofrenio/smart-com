using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Models.BindingModels
{
    public class EditOrderBindingModel
    {
        [Required]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Status")]
        public string Status { get; set; }

        public DateTime ShipmentDate { get; set; }
    }

    public class AddOrderBindingModel
    {
        [Required]
        [Display(Name = "Items")]
        public IEnumerable<Item> Items { get; set; }
    }

    public struct Item
    {
        [Display(Name = "Items count")]
        [Range(0, int.MaxValue)]
        public int ItemsCount { get; set; }

        [Display(Name = "Item price")]
        public double ItemPrice { get; set; }

        [Required]
        [Display(Name = "Item ID")]
        public Guid ItemId { get; set; }
    }
}
