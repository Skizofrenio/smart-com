﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartComTest.API.Common.Repositories.Base.Interface
{
    public interface IIdentityUserRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(string id);
        IEnumerable<T> ToPagedList(IEnumerable<T> items, int pageNumber, int pageSize);
    }
}
