import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import AccountPage from '../views/AccountPage'
import Products from '@/components/Products'
import Orders from '@/components/Orders'
import Basket from '@/components/Basket'
import Users from '@/components/Users'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/account',
    name: 'account',
    component: AccountPage,
    children: [
      {
        path: '/account/product-list/:test',
        name: 'product-list',
        component: Products,
        props: (route) => ({ test: route.params.test }),
        
      },
      {
        path: '/account/orders',
        name: 'orders',
        component: Orders,
      },
      {
        path: '/account/users',
        name: 'users',
        component: Users
      }
    ]
  },
  {
    path: '/basket',
    name: 'basket',
    component: Basket
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
