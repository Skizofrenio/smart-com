﻿using Microsoft.AspNet.Identity.EntityFramework;
using SmartComTest.API.Common.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderItem> OrderItems { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationUser>()
                    .HasOptional(u => u.Customer)
                    .WithRequired(c => c.User)
                    .WillCascadeOnDelete(true);
            modelBuilder.Entity<Customer>()
                    .HasMany(c => c.Orders)
                    .WithRequired(o => o.Customer)
                    .WillCascadeOnDelete(true);
            modelBuilder.Entity<Order>()
                    .HasMany(o => o.Items)
                    .WithRequired(i => i.Order)
                    .WillCascadeOnDelete(true);

            base.OnModelCreating(modelBuilder);
        }

    }
}