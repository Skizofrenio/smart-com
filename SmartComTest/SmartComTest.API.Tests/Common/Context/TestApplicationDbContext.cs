﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Models;
using SmartComTest.API.Tests.Common.DbSet;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartComTest.API.Tests.Common.Context
{
    class TestApplicationDbContext : IApplicationDbContext
    {
        public TestApplicationDbContext()
        {
            Products = new TestProductDbSet();
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Customer> Customers { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DbSet<Order> Orders { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public DbSet<OrderItem> OrderItems { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public void Dispose()
        {
        }

        public void SetModified(object entity)
        {
        }

        public int SaveChanges()
        {
            return 0;
        }
    }
}
