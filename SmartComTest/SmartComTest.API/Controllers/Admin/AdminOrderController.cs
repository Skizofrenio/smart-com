﻿using Microsoft.AspNet.Identity.Owin;
using SmartComTest.API.Common.Datalayers;
using SmartComTest.API.Models;
using SmartComTest.API.Models.BindingModels;
using SmartComTest.API.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace SmartComTest.API.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/admin/order")]
    public class AdminOrderController : AdminBaseController
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
                => _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set
                => _userManager = value;
        }

        [Route("index")]
        [HttpGet]
        public IHttpActionResult GetIndex(int? page, string status = "")
        {
            var items = Datalayer.Orders
                                .GetAll();

            if (status != "")
                items = items.Where(o => o.Status == status);
            items = items.OrderBy(o => o.OrderDate);

            var result = Datalayer.Orders                            
                                .ToPagedList(items, page.Value, 15)
                                .Select(x => new IndexOrderViewModel()
                                {
                                    Id = x.Id,
                                    OrderDate = x.OrderDate,
                                    OrderNumber = x.OrderNumber,
                                    ShipmentDate = x.ShipmentDate,
                                    Status = x.Status,
                                });

            return Json(new OrderPageViewModel()
            {
                Orders = result,
                Pages = (int)Math.Ceiling((decimal)items.Count() / 15)
            });
        }

        [Route("customer")]
        [HttpGet]
        public IHttpActionResult GetByCustomerId(int? page, string customerId)
        {
            var items = Datalayer.Orders.GetAll().Where(o => o.UserId == customerId).OrderBy(o => o.OrderDate);

            var result = Datalayer.Orders
                                .ToPagedList(items, page.Value, 15)
                                .Select(x
                                    => new IndexOrderViewModel()
                                    {
                                        Id = x.Id,
                                        OrderDate = x.OrderDate,
                                        OrderNumber = x.OrderNumber,
                                        ShipmentDate = x.ShipmentDate,
                                        Status = x.Status,
                                    });

            return Json(new OrderPageViewModel()
            {
                Orders = result,
                Pages = (int)Math.Ceiling((decimal)items.Count() / 15)
            });
        }

        [Route("get")]
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            var order = Datalayer.Orders.Get(id);

            return Json(order.ToViewModel());
        }

        [Route("update")]
        [HttpPut]
        public IHttpActionResult Put(EditOrderBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var order = Datalayer.Orders.Get(model.Id);

            if (order.Status == "Выполнен"  || model.Status == "Новый")
                return Conflict();

            order.Status = model.Status;

            if (model.Status == "Выполняется")
                order.ShipmentDate = model.ShipmentDate;

            Datalayer.Orders.Update(order);
            Datalayer.Save();

            return Ok();
        }
    }
}
