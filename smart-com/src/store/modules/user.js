import Axios from 'axios'

export default {
	state: {
		auth: {
			isAuth: false,
			isAdmin: false,
			id: '',
			customerId: '',
			token: '',
			email: '',
			name: '',
			address: '',
			code: '',
			discount: 0.0
		},
		users: [],
		uPages: null,
		auth_pending: false
	},
	getters: {
		AUTH: state => {
			return state.auth
		},
		USERS: state => {
			return state.users
		},
		U_PAGES: state => {
			return state.uPages
		},
		AUTH_PENDING: state => {
			return state.auth_pending
		}
	},
	mutations: {
		SET_USERS: (state, payload) => {
			state.users = payload.Users
			state.uPages = payload.Pages
		},
		SET_AUTH: (state, payload) => {
			state.auth = payload
		},
		REMOVE_USER: (state, id) => {
			state.users.forEach((user, index) => {
				if (user.Id === id)
					state.users.splice(index, 1)
			})
		},
		SET_AUTH_PENDING: (state, payload) => {
			state.auth_pending = payload
		}
	},
	actions: {
		LOGIN: async (context, user) => {
			const requestBody = 
				'grant_type=password' +
				'&username=' + user.login +
				'&password='+ user.password
			
			const config = {
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}
			let tokenResponse  = null
			await Axios.post('https://localhost:44350/Token', requestBody , config)
			.then(function(response) {
				tokenResponse = response
			})
			.catch(function(error) {
				if (typeof error.response !== "undefined") {
					if (typeof error.response.data !== "undefined")
						if (typeof error.response.data.error_description !== "undefined")
							alert(error.response.data.error_description)
				}
				else alert('Server error')
			})
				
			if ( tokenResponse === null) {
			  context.commit('SET_AUTH_PENDING', false)
				return
			}

			let { data, status } = await Axios.get('https://localhost:44350/api/account/user-info', { 
				headers: { 
					Authorization: 'Bearer ' + tokenResponse.data.access_token
				} 	
			})

			if (status === 200 && data != null) {
				let u = {
					isUser: false,
					isAdmin: false,
					id: data.Id,
					customerId: data.CustomerId,
					token: 'Bearer ' + tokenResponse.data.access_token,
					email: data.Email,
					name: data.Name,
					address: data.Address,
					code: data.Code,
					discount: data.Discount
				}
				if (data.Role === 'Admin')
					u.isAdmin = true
				u.isAuth = true
				
				context.commit('SET_AUTH', u)
			}
		},
		REGISTER: async (context, user) => {	
			await Axios.post('https://localhost:44350/api/account/register', user )
				.then(function (response) {
					alert('Успешно')
				  })
				.catch(function (error) {
					let message = ''
					for (let key in error.response.data.ModelState) 
						message += error.response.data.ModelState[key] + '\n'
						
					alert(message)
				})

			context.commit('SET_AUTH_PENDING', false)
		},
		GET_USERS: async (context, page) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}
			let { data } = await Axios.get('https://localhost:44350/api/admin/account/index?page=' + page, config)

			context.commit('SET_USERS', data)
		},
		GET_USER: async (context, id) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}
			let { data } = await Axios.get('https://localhost:44350/api/admin/account/get?id=' + id, config)
			return data
		},
		DELETE_USER: async (context, id) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}
			let { status } = await Axios.delete('https://localhost:44350/api/admin/account/delete?id=' + id, config)
			
			if (status === 200)
				context.commit('REMOVE_USER', id)
		},
		EDIT_USER: async (context, user) => {
			const requestBody = user
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}

			await Axios.put('https://localhost:44350/api/admin/account/update', requestBody, config)
			.then(function (response) {
				alert('Успешно')
			})
			.catch(function (error) {
				let message = ''
				for (let key in error.response.data.ModelState) 
					message += error.response.data.ModelState[key] + '\n'

				alert(message)
			})
		},
		LOGOUT: async (context) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}
			let {status} = await Axios.post('https://localhost:44350/api/account/logout', null, config)

			if (status == 200)
				context.commit('SET_AUTH',  {
					isAuth: false,
					isAdmin: false,
					id: '',
					customerId: '',
					token: '',
					email: '',
					name: '',
					address: '',
					code: '',
					discount: 0.0
				})
		}
	}
}
