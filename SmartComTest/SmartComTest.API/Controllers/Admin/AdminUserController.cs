﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SmartComTest.API.Common.Datalayers;
using SmartComTest.API.Models;
using SmartComTest.API.Models.BindingModels;
using SmartComTest.API.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace SmartComTest.API.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [RoutePrefix("api/admin/account")]
    public class AdminUserController : AdminBaseController
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
                => _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set
                => _userManager = value;
        }

        [Route("index")]
        [HttpGet]
        public IHttpActionResult GetIndex(int? page)
        {
            var result = UserManager.Users
                                    .AsEnumerable()
                                    .OrderBy(u => u.Email)
                                    .Skip(15 * (page.Value - 1))
                                    .Take(15)
                                    .Select(x => new IndexUserViewModel()
                                    {
                                        Id = x.Id,
                                        Email = x.Email,
                                        Roles = UserManager.GetRolesAsync(x.Id).Result,
                                    });

            return Ok(new UserPageViewModel()
            {
                Users = result,
                Pages = (int)Math.Ceiling((decimal)result.Count() / 15)
            });
        }

        [Route("get")]
        [HttpGet]
        public IHttpActionResult Get(string id)
        {
            ApplicationUser user = UserManager.FindById(id);
            
            return Ok(new UserInfoViewModel
                        {
                            Id = user.Id,
                            CustomerId = user.Customer?.UserId,
                            Email = user.Email,
                            Username = user.UserName,
                            Role = UserManager.GetRolesAsync(user.Id).Result[0],
                            Name = user.Customer?.Name,
                            Code = user.Customer?.Code,
                            Address = user.Customer?.Address,
                            Discount = user.Customer?.Discount
                        });
        }

        [Route("update")]
        [HttpPut]
        public IHttpActionResult Put(UpdateUserBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            ApplicationUser user = UserManager.FindById(model.Id);

            if (model.Role == "Admin" || model.CustomerId == "")
            {
                Datalayer.Customers.Delete(user.Id);
                UserManager.RemoveFromRole(user.Id, "User");
                UserManager.AddToRole(user.Id, "Admin");
            }
            else
            {
                var c = Datalayer.Customers.Get(user.Customer.UserId);
                c.Address = model.Address;
                c.Discount = model.Discount;
                c.Name = model.Name;

                Datalayer.Customers.Update(c);
                Datalayer.Save();
            }

            user.UserName = model.Username;
            user.Email = model.Email;

            UserManager.Update(user);

            return Ok();
        }

        [Route("delete")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(string id)
        {
            ApplicationUser user = await UserManager.FindByIdAsync(id);
            if (user != null && user.Id != User.Identity.GetUserId())
            {
                var result = await UserManager.DeleteAsync(user);

                if (result.Succeeded)
                    return Ok();
            }

            return Conflict();
        }
    }
}
