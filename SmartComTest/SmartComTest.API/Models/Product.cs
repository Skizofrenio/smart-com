﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartComTest.API.Models
{
    public class Product
    {
        private string code;

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Code
        {
            get
            {
                if (code == null)
                    code = MakeCode();

                return code;
            }
            private set => code = value;
        }

        public string Name { get; set; }

        public double? Price { get; set; }

        [MaxLength(30)]
        public string Category { get; set; }

        public virtual ICollection<OrderItem> Items { get; set; }

        public Product()
        {
            Items = new List<OrderItem>();
        }

        private string MakeCode()
        {
            Random random = new Random();
            return GetRandomString(random, 2) + 
                '-' + GetRandomString(random, 4) + 
                '-' + random.Next(100) + 
                GetRandomString(random, 2);
        }

        private string GetRandomString(Random random, int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}