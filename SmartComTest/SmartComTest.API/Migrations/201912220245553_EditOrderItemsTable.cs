namespace SmartComTest.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditOrderItemsTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.OrderItems", name: "ItemId", newName: "ProductId");
            RenameIndex(table: "dbo.OrderItems", name: "IX_ItemId", newName: "IX_ProductId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.OrderItems", name: "IX_ProductId", newName: "IX_ItemId");
            RenameColumn(table: "dbo.OrderItems", name: "ProductId", newName: "ItemId");
        }
    }
}
