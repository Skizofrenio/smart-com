import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  const routes = ['account', 'product-list', 'product-edit', 'orders', 'basket', 'users']
  if (routes.find(element => element === to.name) != undefined) {
    if (store.getters.AUTH.isAuth) {
      next()
    }
    else next('login')
  }
  else next()
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
