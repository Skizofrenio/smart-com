import product from './product'
import user from './user'
import order from './order'
import basket from './basket'

export default {
  product,
  user,
  order,
  basket
}
