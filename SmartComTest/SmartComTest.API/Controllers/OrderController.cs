using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SmartComTest.API.Common.Datalayers;
using SmartComTest.API.Models;
using SmartComTest.API.Models.BindingModels;
using SmartComTest.API.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartComTest.API.Controllers
{
    [Authorize(Roles = "User")]
    [RoutePrefix("api/order")]
    public class OrderController : BaseController
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
                => _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set
                => _userManager = value;
        }

        [Route("index")]
        [HttpGet]
        public IHttpActionResult GetIndex(int? page, string status = "")
        {
            var items = Datalayer
                        .Orders
                        .GetAll()
                        .Where(o => o.UserId == User.Identity.GetUserId());

            if (status != "")
                items = items.Where(o => o.Status == status);
            items = items.OrderBy(o => o.OrderDate);

            var result = Datalayer.Orders
                                .ToPagedList(items, page.Value, 15)
                                .Select(x => new IndexOrderViewModel()
                                {
                                    Id = x.Id,
                                    OrderDate = x.OrderDate,
                                    OrderNumber = x.OrderNumber,
                                    ShipmentDate = x.ShipmentDate,
                                    Status = x.Status,
                                }).ToList();

            return Json(new OrderPageViewModel()
            {
                Orders = result,
                Pages = (int)Math.Ceiling((decimal)items.Count() / 15)
            });
        }

        [Route("get")]
        [HttpGet]
        public IHttpActionResult Get(Guid id)
        {
            var order = Datalayer.Orders.Get(id);

            if (order.UserId != User.Identity.GetUserId())
                return Conflict();

            var result = order.ToViewModel();

            return Json(result);
        }

        [Route("add")]
        public IHttpActionResult Post(AddOrderBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());
            var order = new Order()
            {
                UserId = user.Id,
                OrderDate = DateTime.Now,
                Status = "Новый",
            };

            foreach (Item item in model.Items)
            {
                if (!user.Customer.Discount.HasValue)
                    user.Customer.Discount = 0.00;

                order.Items.Add(new OrderItem()
                {
                    ProductId = item.ItemId,
                    ItemPrice = item.ItemPrice - item.ItemPrice * (user.Customer.Discount.Value / 100),
                    ItemsCount = item.ItemsCount
                });
            }

            Datalayer.Orders.Create(order);
            Datalayer.Save();

            var result = new IndexOrderViewModel()
            {
                Id = order.Id,
                OrderDate = order.OrderDate,
                OrderNumber = order.OrderNumber,
                ShipmentDate = order.ShipmentDate,
                Status = order.Status,
            };

            return Json(result);
        }

        [Route("delete")]
        public IHttpActionResult Delete(Guid id)
        {
            if (Datalayer.Orders.Get(id).Status != "Новый")
                return Conflict();

            Datalayer.Orders.Delete(id);
            Datalayer.Save();

            return Ok();
        }
    }
}
