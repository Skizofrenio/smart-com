﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Common.Datalayers
{
    public abstract class BaseDatalayer : IDisposable
    {
        protected bool disposed = false;

        protected IApplicationDbContext db;

        public BaseDatalayer(IApplicationDbContext context)
        {
            db = context;
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}