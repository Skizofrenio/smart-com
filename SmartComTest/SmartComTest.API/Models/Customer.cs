﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Models
{
    public partial class Customer
    {
        private string code;

        [Key, ForeignKey("User")]
        public string UserId { get; set; }

        public string Name { get; set; }

        public string Code
        {
            get
            {
                if (code == null)
                    code = MakeCode();

                return code;
            }

            private set => code = value;
        }

        public string Address { get; set; }

        public double? Discount { get; set; }

        public virtual ApplicationUser User { get; set; }

        public virtual ICollection<Order> Orders { get; set; }

        public Customer()
        {
            Orders = new List<Order>();
        }

        private string MakeCode()
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return new string(Enumerable.Repeat(chars, 4)
              .Select(s => s[random.Next(s.Length)]).ToArray()) + '-' + DateTime.Now.Year.ToString();
        }
    }
}