import Axios from 'axios'

export default {
	state: {
		orders: [],
		oPages: null,
	},
	getters: {
		ORDERS: state => {
			return state.orders
		},
		O_PAGES: state => {
			return state.oPages
		}
	},
	mutations: {
		ADD_ORDER: (state, payload) => {
			state.orders.push(payload)
		},
		SET_ORDERS: (state, payload) => {
			state.orders = payload.Orders
			state.oPages = payload.Pages
		},
		REMOVE_ORDER: (state, id) => {
			state.orders.forEach((order, index) => {
        if (order.Id === id) 
          state.orders.splice(index, 1)				
			});
		},
		UPDATE_ORDER: (state, payload) => {
			state.orders.forEach((order) => {
				if (order.Id === payload.Id) {
					order.Status = payload.Status
					order.ShipmentDate = payload.ShipmentDate
				}
			})
		}
	},
	actions: {
		SAVE_ORDER: async (context, payload) => {
			const requestBody = {
				items: payload.map((item) => {
					return {
						ItemsCount: item.ItemsCount,
						ItemPrice: item.Product.Price,
						ItemId: item.Product.Id
					}
				})
			}
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}
			
			let {data, status} = await Axios.post('https://localhost:44350/api/order/add', requestBody, config)

			if (status === 200) {
				context.commit('ERASE_BASKET')
				context.commit('ADD_ORDER', data)
			}
		},
		GET_ORDERS: async (context, page) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}

			let url = 'https://localhost:44350/api/order/index?page='
			if (context.getters.AUTH.isAdmin) 
				url = 'https://localhost:44350/api/admin/order/index?page='
				
				let { data } = await Axios.get(url + page, config)

				context.commit('SET_ORDERS', data)
		},
		GET_ORDERS_F: async (context, payload) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}

			let url = 'https://localhost:44350/api/order/index?page='
			if (context.getters.AUTH.isAdmin) 
				url = 'https://localhost:44350/api/admin/order/index?page='
				
				let { data } = await Axios.get(url + payload.page + '&status=' + payload.status, config)

				context.commit('SET_ORDERS', data)
		},
		GET_ORDER: async (context, id) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}
			
			let url = 'https://localhost:44350/api/order/get?id='
			if (context.getters.AUTH.isAdmin) 
				url = 'https://localhost:44350/api/admin/order/get?id='

			let { data } = await Axios.get(url + id, config)
			return data
		},
		DELETE_ORDER: async (context, payload) => {
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}
			let {status} = await Axios.delete('https://localhost:44350/api/order/delete?id=' + payload.Id, config)

			if (status === 200) 
				context.commit('REMOVE_ORDER', payload.Id)
		},
		EDIT_ORDER: async (context, payload) => {
			const requestBody = {
				Id: payload.Id,
				Status: payload.Status,
				ShipmentDate: payload.ShipmentDate
			}
			const config = {
				headers: {
					'Authorization': context.getters.AUTH.token
				}
			}

			let {status} = await Axios.put('https://localhost:44350/api/admin/order/update', requestBody, config)

			if (status === 200) 
				context.commit('UPDATE_ORDER', payload)
		}
	}
}
