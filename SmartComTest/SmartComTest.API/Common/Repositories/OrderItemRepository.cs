﻿using SmartComTest.API.Common.Repositories.Base.Interface;
using SmartComTest.API.Common.Repositories.Base;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using SmartComTest.API.Common.Context;

namespace SmartComTest.API.Common.Repositories
{
    public class OrderItemRepository : RepositoryBase<OrderItem>, IUserOrderRepository<OrderItem>
    {
        public OrderItemRepository(IApplicationDbContext context)
            : base(context)
        {
        }

        public override void Create(OrderItem item)
        {
            db.OrderItems.Add(item);
        }

        public override void Delete(Guid id)
        {
            OrderItem orderItem = db.OrderItems.Where(oi => oi.Id == id).FirstOrDefault();
            if (orderItem != null)
                db.OrderItems.Remove(orderItem);
        }

        public override OrderItem Get(Guid id)
        {
            return db.OrderItems
                        .Where(oi => oi.Id == id)
                        .Include(oi => oi.Order)
                        .Include(oi => oi.Product)
                        .FirstOrDefault();
        }

        public override IEnumerable<OrderItem> GetAll()
        {
            return db.OrderItems;
        }

        public override void Update(OrderItem item)
        {
            db.SetModified(item);
        }
    }
}