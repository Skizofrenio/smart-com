﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace SmartComTest.API.Models.BindingModels
{
    public class AddProductBindingModel
    {

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Price")]
        public double Price { get; set; }

        [MaxLength(30)]
        [Display(Name = "Category")]
        public string Category { get; set; }
    }

    public class UpdateBindingModel
    {
        [Required]
        [Display(Name = "Id")]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Price")]
        public double Price { get; set; }

        [Required]
        [MaxLength(30)]
        [Display(Name = "Category")]
        public string Category { get; set; }
    }
}