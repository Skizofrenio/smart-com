﻿using SmartComTest.API.Common.Repositories.Base.Interface;
using SmartComTest.API.Common.Repositories.Base;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using SmartComTest.API.Common.Context;

namespace SmartComTest.API.Common.Repositories
{
    public class CustomerRepository : RepositoryIdentityBase<Customer>
    {
        public CustomerRepository(IApplicationDbContext context)
            :base(context)
        {
        }

        public override void Create(Customer item)
        {
            db.Customers.Add(item);
        }

        public override void Delete(string id)
        {
            Customer customer = db.Customers.FirstOrDefault(p => p.UserId == id);
            if (customer != null)
                db.Customers.Remove(customer);
        }

        public override Customer Get(string id)
        {
            return db.Customers
                        .Include(c => c.User)
                        .Include(c => c.Orders)
                        .FirstOrDefault(c => c.UserId == id);
        }

        public override IEnumerable<Customer> GetAll()
        {
            return db.Customers;
        }

        public override void Update(Customer item)
        {
            db.SetModified(item);
        }
    }
}