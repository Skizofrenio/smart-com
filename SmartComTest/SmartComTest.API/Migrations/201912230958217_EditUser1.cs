namespace SmartComTest.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditUser1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "Enabled");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Enabled", c => c.Boolean(nullable: false));
        }
    }
}
