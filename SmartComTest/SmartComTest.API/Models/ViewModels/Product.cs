﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Models.ViewModels
{
    public class ProductViewModel
    {
        public Guid Id { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public double? Price { get; set; }

        public string Category { get; set; }
    }
    
    public class ProductPageViewModel
    {
        public IEnumerable<ProductViewModel> Products { get; set; }

        public int Pages { get; set; }
    }
}