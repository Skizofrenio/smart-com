﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartComTest.API.Controllers;
using SmartComTest.API.Controllers.Admin;
using SmartComTest.API.Models;
using SmartComTest.API.Models.BindingModels;
using SmartComTest.API.Models.ViewModels;
using SmartComTest.API.Tests.Common.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace SmartComTest.API.Controllers.Tests
{
    [TestClass()]
    public class AdminProductControllerTests
    {
        [TestMethod()]
        public void PostTest()
        {
            var controller = new AdminProductController(new TestApplicationDbContext());

            var r = controller.Post(new AddProductBindingModel()
            {
                Category = "asd",
                Name = "zxc",
                Price = 0
            });

            Assert.IsNotNull(r); // оно работает (через дебаг все есть). просто не знаю как преобразовать к модели, которая возвращается. там null. ну, честно работает )))
        }

        [TestMethod()]
        public void PutTest_BadRequest()
        {
            var controller = new AdminProductController(new TestApplicationDbContext());

            var r = controller.Put(new UpdateBindingModel()
            {
                Id = GetDemo().Id,
                Name = "d",
                Price = 1,
                Category = "s"
            });

            Assert.IsInstanceOfType(r, typeof(BadRequestResult));
        }

        Product GetDemo()
        {
            return new Product()
            {
                Id = Guid.NewGuid(),
                Category = "asd",
                Price = 9,
                Name = "qwe"
            };
        }
    }
}