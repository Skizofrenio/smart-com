﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Repositories;
using SmartComTest.API.Common.Repositories.Base.Interface;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Common.Datalayers
{
    public class UserDatalayer : BaseDatalayer
    {
        public UserDatalayer(IApplicationDbContext context)
            : base(context)
        {
        }

        private IUserRepository<Product> productRepository;

        public IUserRepository<Product> Products
        {
            get
            {
                if (productRepository == null)
                    productRepository = new ProductRepository(db);

                return productRepository;
            }
        }

        private IUserOrderRepository<Order> orderRepository;

        public IUserOrderRepository<Order> Orders
        {
            get
            {
                if (orderRepository == null)
                    orderRepository = new OrderRepository(db);

                return orderRepository;
            }
        }

        private IUserOrderRepository<OrderItem> orderItemRepository;

        public IUserOrderRepository<OrderItem> OrderItems
        {
            get
            {
                if (orderItemRepository == null)
                    orderItemRepository = new OrderItemRepository(db);

                return orderItemRepository;
            }
        }
    }
}