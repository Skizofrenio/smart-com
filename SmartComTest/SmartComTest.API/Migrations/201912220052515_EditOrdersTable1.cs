namespace SmartComTest.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditOrdersTable1 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Orders", name: "CustomerId", newName: "UserId");
            RenameIndex(table: "dbo.Orders", name: "IX_CustomerId", newName: "IX_UserId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Orders", name: "IX_UserId", newName: "IX_CustomerId");
            RenameColumn(table: "dbo.Orders", name: "UserId", newName: "CustomerId");
        }
    }
}
