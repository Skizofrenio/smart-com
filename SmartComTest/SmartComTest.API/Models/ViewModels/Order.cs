﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Models.ViewModels
{
    public class IndexOrderViewModel
    {
        public Guid Id { get; set; }

        public DateTime? OrderDate { get; set; }

        public DateTime? ShipmentDate { get; set; }

        public long OrderNumber { get; set; }

        public string Status { get; set; }
    }

    public class OrderViewModel
    {
        public Guid Id { get; set; }

        public DateTime? OrderDate { get; set; }

        public DateTime? ShipmentDate { get; set; }

        public long OrderNumber { get; set; }

        public string Status { get; set; }

        public UserInfoViewModel User { get; set; }

        public IEnumerable<OrderItemViewModel> Items { get; set; }
    }

    public class OrderItemViewModel
    {
        public Guid Id { get; set; }

        public int ItemsCount { get; set; }

        public double ItemPrice { get; set; }

        public ProductViewModel Product { get; set; }
    }

    public class OrderPageViewModel
    {
        public IEnumerable<IndexOrderViewModel> Orders { get; set; }

        public int Pages { get; set; }
    }
}