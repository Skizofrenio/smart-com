﻿using System;
using System.Collections.Generic;

namespace SmartComTest.API.Models.ViewModels
{
    public class UserInfoViewModel
    {
        public string Id { get; set; }

        public string CustomerId { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public string Role { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Address { get; set; }

        public double? Discount { get; set; }
    }

    public class IndexUserViewModel
    {
        public string Id { get; set; }

        public string Email { get; set; }

        public IList<string> Roles { get; set; }
    }

    public class UserPageViewModel
    {
        public IEnumerable<IndexUserViewModel> Users { get; set; }

        public int Pages { get; set; }
    }
}
