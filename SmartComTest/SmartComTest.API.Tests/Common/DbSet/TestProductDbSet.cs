﻿using SmartComTest.API.Models;
using SmartComTest.API.Tests.Common.DbSet.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartComTest.API.Tests.Common.DbSet
{
    class TestProductDbSet : BaseTestDbSet<Product>
    {
        public override Product Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (Guid)keyValues.Single());
        }
    }
}
