import Axios from 'axios'

export default {
	state: {
		items: []
	},
	getters: {
		ITEMS: state => {
			return state.items
		}
	},
	mutations: {
		ADD_ITEM: (state, payload) => {
			state.items.push(payload)
		},
		REMOVE_ITEM: (state, payload) => {
      state.items.forEach((item, index) => {
        if (item.id === payload.id) {
          state.items.splice(index, 1)
        }
      })
    },
    EDIT_ITEM: (state, payload) => {
      state.items.forEach((item, index) => {
        if (item.id === payload.id) {
          item = payload
        }
      })
		},
		ERASE_BASKET: state => {
			state.items = []
		}
	},
	actions: {
		SET_ITEM: (context, payload) => {
			const item = {
				ItemsCount: payload.count,
				Product: payload.product,
				id: +context.getters.ITEMS.length + +1
			}
			context.commit('ADD_ITEM', item)
		},
		UPDATE_ITEM: (context, payload) => {
			context.commit('EDIT_ITEM', payload)
		},
		DELETE_ITEM: (context, payload) => {
			context.commit('REMOVE_ITEM', payload)
		},
	}
}
