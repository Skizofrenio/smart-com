﻿using SmartComTest.API.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public DateTime? OrderDate { get; set; }

        public DateTime? ShipmentDate { get; set; }

        private long orderNumber;

        public long OrderNumber
        {
            get
            {
                if (orderNumber == 0)
                    orderNumber = MakeNum();

                return orderNumber;
            }
            private set => orderNumber = value;
        }

        public string Status { get; set; }

        [ForeignKey("Customer")]
        public string UserId { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual ICollection<OrderItem> Items { get; set; }

        public Order()
        {
            Items = new List<OrderItem>();
        }

        private long MakeNum()
        {
            byte[] buffer = new byte[sizeof(long)];
            Random r = new Random();
            r.NextBytes(buffer);
            long rand = BitConverter.ToInt64(buffer, 0);
            return Math.Abs(rand % (long.MaxValue - DateTime.Now.Millisecond + 1) + DateTime.Now.Millisecond);
        }

        public OrderViewModel ToViewModel()
        {
            return new OrderViewModel()
            {
                Id = Id,
                OrderDate = OrderDate,
                OrderNumber = OrderNumber,
                ShipmentDate = ShipmentDate,
                Status = Status,
                User = new UserInfoViewModel()
                {
                    Id = Customer.User.Id,
                    CustomerId = UserId,
                    Name = Customer.Name,
                    Code = Customer.Code,
                    Email = Customer.User.Email,
                    Username = Customer.User.UserName,
                    Address = Customer.Address,
                    Discount = Customer.Discount
                },
                Items = Items
                        .Select(x
                            => new OrderItemViewModel()
                            {
                                Id = x.Id,
                                ItemPrice = x.ItemPrice,
                                ItemsCount = x.ItemsCount,
                                Product = new ProductViewModel()
                                {
                                    Id = x.Product.Id,
                                    Category = x.Product.Category,
                                    Code = x.Product.Code,
                                    Name = x.Product.Name,
                                    Price = x.Product.Price
                                },
                            }),
            };
        }
    }
}