﻿using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartComTest.API.Common.Context
{
    public interface IApplicationDbContext : IDisposable
    {
        DbSet<Product> Products { get; set; }
        DbSet<Customer> Customers { get; set; }

        DbSet<Order> Orders { get; set; }

        DbSet<OrderItem> OrderItems { get; set; }

        void SetModified(object entity);

        int SaveChanges();
    }
}
