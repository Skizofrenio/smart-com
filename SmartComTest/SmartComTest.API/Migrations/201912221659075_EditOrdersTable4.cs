namespace SmartComTest.API.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditOrdersTable4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Orders", "OrderNumber");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "OrderNumber", c => c.Long(nullable: false, identity: true));
        }
    }
}
