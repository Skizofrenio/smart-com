﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Datalayers;
using SmartComTest.API.Models;
using SmartComTest.API.Models.BindingModels;
using SmartComTest.API.Models.ViewModels;
using SmartComTest.API.Providers;
using SmartComTest.API.Results;

namespace SmartComTest.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        private ApplicationUserManager _userManager;

        private AdminDatalayer Datalayer;

        public ApplicationUserManager UserManager
        {
            get
                => _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set
                => _userManager = value;
        }

        public AccountController()
        {
            Datalayer = new AdminDatalayer(new ApplicationDbContext());
        }

        public AccountController(IApplicationDbContext context)
        {
            Datalayer = new AdminDatalayer(context);
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("user-info")]
        public UserInfoViewModel GetUserInfo()
        {
            ApplicationUser user = UserManager.FindById(User.Identity.GetUserId());

            return new UserInfoViewModel
            {
                Id = user.Id,
                CustomerId = user.Customer?.UserId,
                Email = user.Email,
                Username = user.UserName,
                Role = UserManager.GetRolesAsync(user.Id).Result[0],
                Name = user.Customer?.Name,
                Code = user.Customer?.Code,
                Address = user.Customer?.Address,
                Discount = user.Customer?.Discount
            };
        }

        [Route("logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        [AllowAnonymous]
        [Route("register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email,
            };

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
                return GetErrorResult(result);

            var currentUser = UserManager.FindByEmail(user.Email);

            UserManager.AddToRole(currentUser.Id, "User");

            var customer = new Customer()
            {
                Address = model.Address,
                Discount = null,
                Name = model.Name,
                UserId = currentUser.Id
            };

            Datalayer.Customers.Create(customer);
            Datalayer.Save();

            return Ok();
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }


        #endregion
    }
}
