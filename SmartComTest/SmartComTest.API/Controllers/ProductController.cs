﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Datalayers;
using SmartComTest.API.Models;
using SmartComTest.API.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace SmartComTest.API.Controllers
{
    [RoutePrefix("api/product")]
    public class ProductController : BaseController
    {
        public ProductController()
            : base()
        {

        }

        public ProductController(IApplicationDbContext context)
            : base(context)
        {

        }

        [Route("index")]
        public IHttpActionResult GetIndex(int? page)
        {
            var items = Datalayer.Products.GetAll().OrderBy(p => p.Name);

            var result = Datalayer.Products
                                .ToPagedList(items, page.Value, 15)
                                .Select(x => new ProductViewModel()
                                {
                                    Id = x.Id,
                                    Name = x.Name,
                                    Category = x.Category,
                                    Code = x.Code,
                                    Price = x.Price,
                                }).ToList();

            return Json(new ProductPageViewModel
            {
                Products = result,
                Pages = (int)Math.Ceiling((decimal)items.Count() / 15)
            });
        }

        [Route("search")]
        [HttpGet]
        public IHttpActionResult Search(string filter, string searchString)
        {
            var items = Datalayer.Products.GetAll();

            if (searchString != null)
            {
                if (filter == "Код")
                    items = items.Where(p => p.Code == searchString);
                if (filter == "Категория")
                    items = items.Where(p => p.Category == searchString);
                if (filter == "Название")
                    items = items.Where(p => p.Name.ToLower().Contains(searchString.ToLower()));
            }

            items = items.OrderBy(p => p.Name);
            var result = Datalayer.Products
                                .ToPagedList(items, 1, 15)
                                .Select(x => new ProductViewModel()
                                {
                                    Id = x.Id,
                                    Name = x.Name,
                                    Category = x.Category,
                                    Code = x.Code,
                                    Price = x.Price,
                                }).ToList();

            return Json(new ProductPageViewModel
            {
                Products = result,
                Pages = (int)Math.Ceiling((decimal)items.Count() / 15)
            });
        }
    }
}
