﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Repositories.Base.Interface;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartComTest.API.Common.Repositories.Base
{
    public abstract class RepositoryIdentityBase<T> : IIdentityUserRepository<T>, IIdentityRepository<T>
        where T : class
    {
        protected IApplicationDbContext db;

        public RepositoryIdentityBase(IApplicationDbContext context)
        {
            db = context;
        }

        public abstract void Create(T item);

        public abstract void Delete(string id);

        public abstract T Get(string id);

        public abstract IEnumerable<T> GetAll();

        public abstract void Update(T item);

        public IEnumerable<T> ToPagedList(IEnumerable<T> items, int pageNumber, int pageSize)
            => items
                .Skip(pageSize * (pageNumber - 1)).Take(pageSize);
    }
}