﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartComTest.API.Common.Repositories.Base.Interface
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T Get(Guid id);
        void Create(T item);
        void Update(T item);
        void Delete(Guid id);
        IEnumerable<T> ToPagedList(IEnumerable<T> items, int pageNumber, int pageSize);
    }
}
