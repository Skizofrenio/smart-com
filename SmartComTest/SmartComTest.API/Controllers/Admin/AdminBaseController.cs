﻿using SmartComTest.API.Common.Context;
using SmartComTest.API.Common.Datalayers;
using SmartComTest.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SmartComTest.API.Controllers.Admin
{
    public class AdminBaseController : ApiController
    {
        protected AdminDatalayer Datalayer;

        public AdminBaseController()
        {
            Datalayer = new AdminDatalayer(new ApplicationDbContext());
        }

        public AdminBaseController(IApplicationDbContext context)
        {
            Datalayer = new AdminDatalayer(context);
        }
    }
}
